import csv
import numpy as np


def find(n, k, m):
    en = 0
    for j in range(n):
        for i in range(k):
            lij = m[j, i]
            if lij != 0:
                print(lij / (n - 1))
                en -= (lij / (n - 1)) * np.log2(lij / (n - 1))
    return en


def task(data):
    n, k = np.array([[float(num) for num in row] for row in data]).shape
    en = find(n, k, np.array([[float(num) for num in row] for row in data]))
    return round(en, 1)


def f(file):
    print(task(list(csv.reader(file))))


if __name__ == '__main__':
    with open('example.csv', 'r') as file:
        f(file)
