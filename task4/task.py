import math
import numpy as np

s = sorted(set(i1 + i2 for i1 in range(1, 7) for i2 in range(1, 7)))
p = sorted(set(i1 * i2 for i1 in range(1, 7) for i2 in range(1, 7)))
c = np.zeros((len(s), len(p)))


def fillMatrix():
    for i in range(1, 7):
        for j in range(1, 7):
            sum_val = i + j
            product_val = i * j
            c[s.index(sum_val), p.index(product_val)] += 1


def findEntropyA():
    entropy_A = 0
    for i in range(c.shape[0]):
        sum_A = sum(c[i])
        entropy_A += sum_A / 36 * math.log2(sum_A / 36)
    return entropy_A


def findEntropyB():
    entropy_B = 0
    for j in range(c.shape[1]):
        sum_B = 0
        for i in range(c.shape[0]):
            sum_B += c[i, j]
        entropy_B += sum_B / 36 * math.log2(sum_B / 36)
    return entropy_B


def findEntropyA_B():
    p = c / 36
    entropy_A_B = 0
    for i in range(p.shape[0]):
        for j in range(p.shape[1]):
            if p[i, j] != 0:
                entropy_A_B += p[i, j] * (math.log2(p[i, j]))
    return entropy_A_B


def task():
    fillMatrix()
    entropy_A = findEntropyA()
    entropy_B = findEntropyB()
    entropy_AB = findEntropyA_B()
    entropy_aB = entropy_AB - entropy_A
    information_AB = entropy_B - entropy_aB
    return [round(value, 2) for value in [-entropy_AB, -entropy_A, -entropy_B, -entropy_aB, -information_AB]]


if __name__ == '__main__':
    print(task())
