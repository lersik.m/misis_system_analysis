import numpy as np
import json


def function():
    a = [1, [2, 3], 4, [5, 6, 7], 8, 9, 10]
    b = [[1, 2], [3, 4, 5], 6, 7, 9, [8, 10]]
    c = [3, [1, 4], 2, 6, [5, 7, 8], [9, 10]]
    print(task(a, b, c))


def f1(r, ra):
    ran = []
    for rank in r:
        order_list = [0] * len(ra)
        for i, item in enumerate(rank):
            if isinstance(item, list):
                for sub_item in item:
                    order_list[ra[sub_item]] = i + 1
            else:
                order_list[ra[item]] = i + 1
        ran.append(order_list)
    return ran


def f2(s, r):
    return np.var(s) * r / (r - 1)


def f3(n, r):
    return (n ** 2) * ((r ** 3 - r) / 12) / (r - 1)


def task(*ran):
    n = len(ran)
    r = {}
    rc = 0

    for element in ran[0]:
        if isinstance(element, list):
            for sub_elem in element:
                r[sub_elem] = rc
                rc += 1
        else:
            r[element] = rc
            rc += 1

    rank_matrix = f1(ran, r)
    rank_array = np.array(rank_matrix)

    return format(f2(np.sum(rank_array, axis=0), rc) / f3(n, rc), ".2f")


if __name__ == '__main__':
    function()
