import csv
import numpy as np
from io import StringIO


def function(data):
    arr = []
    for i in data:
        if i[0] not in arr:
            arr.append(i[0])
        if i[1] not in arr:
            arr.append(i[1])
    return arr


def chetomytim(arr, data):
    newArray = np.zeros((len(arr), 5))
    for i in data:
        point = list(map(int, i))
        newArray[point[0] - 1][0] += 1
        newArray[point[1] - 1][1] += 1
        for j in data:
            point2 = list(map(int, j))
            if point2[0] == point[1]:
                newArray[point[0] - 1][2] += 1
                newArray[point2[1] - 1][3] += 1
            if point2[0] == point[0] and point2[1] != point[1]:
                newArray[point2[1] - 1][4] += 1
    return newArray


def chetoprintim(matrix):
    csv_string = ""
    np.savetxt('output.csv', matrix, delimiter=',', fmt='%d')
    with open('output.csv', 'r') as file:
        csv_string = file.read()
    return csv_string


def task(csv_string):
    return chetoprintim(chetomytim(function(np.genfromtxt(StringIO(csv_string), delimiter=',', dtype=float)), np.genfromtxt(StringIO(csv_string), delimiter=',', dtype=float)))


if __name__ == '__main__':
    path = 'task.csv'
    with open(path, 'r') as file:
        reader_csv = file.read()
        task(reader_csv)
