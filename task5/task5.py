import json
import numpy as np


def function_for_gt_clusters_from_str(c):
    cl = []
    for i in c:
        if isinstance(i, list):
            cl.append(i)
        else:
            cl.append([i])
    return cl


def gt_clusters_from_str(str_json):
    return function_for_gt_clusters_from_str(json.loads(str_json))


def ff(cl, m):
    worse = []
    for cluster in cl:
        for worse_elem in worse:
            for elem in cluster:
                m[elem - 1][worse_elem - 1] = 0
        worse.extend(cluster)
    return worse


def gt_matrix_from_expert(str_json: str):
    clusters = gt_clusters_from_str(str_json)
    n = sum(len(cluster) for cluster in clusters)
    matrix = np.ones((n, n), dtype=int)
    worse = ff(clusters, matrix)
    return matrix


def gt_AND_matrix(matrix1, matrix2):
    return np.multiply(matrix1, matrix2)


def gt_OR_matrix(matrix1, matrix2):
    return np.maximum(matrix1, matrix2)


def gt_clusters(matrix, est1, est2):
    clusters = {}
    exclude = set()

    rows, cols = matrix.shape
    for row in range(rows):
        if row + 1 in exclude:
            continue
        clusters[row + 1] = [row + 1]
        for col in range(row + 1, cols):
            if matrix[row][col] == 0:
                clusters[row + 1].append(col + 1)
                exclude.add(col + 1)

    result = []
    for k in clusters:
        if not result:
            result.append(clusters[k])
            continue

        for i, elem in enumerate(result):
            if (
                    np.sum(est1[elem[0] - 1]) == np.sum(est1[k - 1])
                    and np.sum(est2[elem[0] - 1]) == np.sum(est2[k - 1])
            ):
                result[i].extend(clusters[k])
                break

            if (
                    np.sum(est1[elem[0] - 1]) < np.sum(est1[k - 1])
                    or np.sum(est2[elem[0] - 1]) < np.sum(est2[k - 1])
            ):
                result = result[:i] + clusters[k] + result[i:]
                break

        result.append(clusters[k])

    final = [r[0] if len(r) == 1 else r for r in result]
    return str(final)


def tsk(string1, string2):
    mx1 = gt_matrix_from_expert(string1)
    mx2 = gt_matrix_from_expert(string2)

    mx_and = gt_AND_matrix(mx1, mx2)
    mx_and_t = gt_AND_matrix(np.transpose(mx1), np.transpose(mx2))

    mx_or = gt_OR_matrix(mx_and, mx_and_t)
    clusters = gt_clusters(mx_or, mx1, mx2)
    return clusters


if __name__ == "__main__":
    string1 = '[1,[2,3],4,[5,6,7],8,9,10]'
    string2 = '[[1,2],[3,4,5],6,7,9,[8,10]]'
    print(tsk(string1, string2))
